import Vue from 'vue'
import App from './App.vue'
require ('./assets/main.scss')

$(document).ready(function() {
  $('select').material_select();
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
